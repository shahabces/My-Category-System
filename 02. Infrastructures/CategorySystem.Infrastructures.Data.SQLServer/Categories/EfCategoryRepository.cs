﻿using CategorySystem.Core.Contracts.Categories;
using CategorySystem.Core.Entities.Categories;
using CategorySystem.Core.Entities.SubCategories;
using CategorySystem.Core.Entities.Tags;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CategorySystem.Infrastructures.Data.SQLServer.Categories
{
    public class EfCategoryRepository : ICategoryRepository
    {
        private readonly CategorySystemDbContext _categorySystemDbContext;

        public EfCategoryRepository(CategorySystemDbContext categorySystemDbContext)
        {
            _categorySystemDbContext = categorySystemDbContext;
        }

        public List<SubCategory> GetAllSubCategories(int id)
        {
            var category = _categorySystemDbContext.Categories.Find(id);

            if (category==null)
            {
                throw new Exception("Category not found");
            }

            _categorySystemDbContext.Entry(category).Reference(p => p.Tag).Load();

            if (category.Tag==null)
            {
                return new List<SubCategory>();
            }

            _categorySystemDbContext.Entry(category.Tag).Collection(c => c.SubCategories).Load();

            return category.Tag.SubCategories;
        }

        public Category SetTag(int id, Tag tag)
        {
            var category = _categorySystemDbContext.Categories.Find(id);

            if (category == null)
            {
                throw new Exception("Category not found");
            }

            category.Tag = tag;
            _categorySystemDbContext.SaveChanges();
            return category;
        }
    }
}
