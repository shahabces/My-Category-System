﻿using CategorySystem.Core.Contracts.Tags;
using CategorySystem.Core.Entities.Tags;
using System.Collections.Generic;
using System.Linq;

namespace CategorySystem.Infrastructures.Data.SQLServer.Tags
{
    public class EfTagRepository : ITagRepository
    {
        private readonly CategorySystemDbContext _categorySystemDbContext;

        public EfTagRepository(CategorySystemDbContext categorySystemDbContext)
        {
            _categorySystemDbContext = categorySystemDbContext;
        }
        public Tag Add(Tag tag)
        {
            _categorySystemDbContext.Add(tag);
            _categorySystemDbContext.SaveChanges();
            return tag;
        }

        public Tag Find(int id)
        {
            return _categorySystemDbContext.Tags.Find(id);
        }

        public List<Tag> GetAll()
        {
            return _categorySystemDbContext.Tags.ToList();
        }
    }
}
