﻿using CategorySystem.Core.Entities.Categories;
using CategorySystem.Core.Entities.SubCategories;
using CategorySystem.Core.Entities.Tags;
using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics.CodeAnalysis;

namespace CategorySystem.Infrastructures.Data.SQLServer
{
    public class CategorySystemDbContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<Tag> Tags { get; set; }

        public CategorySystemDbContext([NotNull] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        }
    }
}
