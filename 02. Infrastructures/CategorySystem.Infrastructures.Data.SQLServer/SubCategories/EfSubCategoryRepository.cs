﻿using CategorySystem.Core.Contracts.SubCategories;
using CategorySystem.Core.Entities.SubCategories;
using CategorySystem.Core.Entities.Tags;
using System;

namespace CategorySystem.Infrastructures.Data.SQLServer.SubCategories
{
    public class EfSubCategoryRepository:ISubCategoryRepository
    {
        private readonly CategorySystemDbContext _categorySystemDbContext;

        public EfSubCategoryRepository(CategorySystemDbContext categorySystemDbContext)
        {
            _categorySystemDbContext = categorySystemDbContext;
        }
        public SubCategory SetTag(int id, Tag tag)
        {
            var subCategory = _categorySystemDbContext.SubCategories.Find(id);

            if (subCategory == null)
            {
                throw new Exception("Sub category not found");
            }

            subCategory.Tag = tag;
            _categorySystemDbContext.SaveChanges();
            return subCategory;
        }
    }
}
