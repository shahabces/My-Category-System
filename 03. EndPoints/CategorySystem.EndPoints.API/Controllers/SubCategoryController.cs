﻿using CategorySystem.Core.Contracts.SubCategories;
using CategorySystem.Core.Contracts.SubCategories.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace CategorySystem.EndPoints.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SubCategoryController:ControllerBase
    {
        private readonly ISubCategoryApplicationService _subCategoryApplicationService;

        public SubCategoryController(ISubCategoryApplicationService subCategoryApplicationService)
        {
            _subCategoryApplicationService = subCategoryApplicationService;
        }

        [Route("setTag")]
        [HttpPost]
        public SetTagForSubCategoryResult SetTag(SetTagForSubCategoryDto setTagForSubCategoryDto)
        {
            return _subCategoryApplicationService.SetTagForSubCategory(setTagForSubCategoryDto);
        }
    }
}
