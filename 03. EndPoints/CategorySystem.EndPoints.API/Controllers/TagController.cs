﻿using CategorySystem.Core.Contracts.Tags;
using CategorySystem.Core.Contracts.Tags.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CategorySystem.EndPoints.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TagController:ControllerBase
    {
        private readonly ITagApplicationService _tagApplicationService;

        public TagController(ITagApplicationService tagApplicationService)
        {
            _tagApplicationService = tagApplicationService;
        }

        [Route("add")]
        [HttpPost]
        public TagResult AddTag(TagDto tagDto)
        {
            return _tagApplicationService.AddTag(tagDto);
        }

        [Route("getAll")]
        [HttpGet]
        public List<TagResult> GetAll()
        {
            return _tagApplicationService.GetAllTags();
        }
    }
}
