﻿using CategorySystem.Core.Contracts.Categories;
using CategorySystem.Core.Contracts.Categories.Dtos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategorySystem.EndPoints.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryApplicationService _categoryApplicationService;

        public CategoryController(ICategoryApplicationService categoryApplicationService)
        {
            _categoryApplicationService = categoryApplicationService;
        }

        [Route("setTag")]
        [HttpPost]
        public SetTagForCategoryResult SetTag(SetTagForCategoryDto setTagForCategoryDto)
        {
            return _categoryApplicationService.SetTagForCategory(setTagForCategoryDto);
        }

        [Route("getAllSubCategories")]
        [HttpGet]
        public GetAllSubCategoriesResult GetAllSubCategories(int id)
        {
            return _categoryApplicationService.GetAllSubCategories(new GetAllSubCategoriesDto
            {
                CategoryId = id
            });
        }
    }
}
