using AutoMapper;
using CategorySystem.Core.ApplicationServices;
using CategorySystem.Core.ApplicationServices.Categories;
using CategorySystem.Core.ApplicationServices.SubCategories;
using CategorySystem.Core.ApplicationServices.Tags;
using CategorySystem.Core.Contracts.Categories;
using CategorySystem.Core.Contracts.SubCategories;
using CategorySystem.Core.Contracts.Tags;
using CategorySystem.Infrastructures.Data.SQLServer;
using CategorySystem.Infrastructures.Data.SQLServer.Categories;
using CategorySystem.Infrastructures.Data.SQLServer.SubCategories;
using CategorySystem.Infrastructures.Data.SQLServer.Tags;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategorySystem.EndPoints.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSwaggerGen();

            services.AddDbContext<CategorySystemDbContext>(c => c.UseSqlServer(Configuration.GetConnectionString("CategorySystemCnn")));

            services.AddScoped<ICategoryRepository, EfCategoryRepository>();
            services.AddScoped<ISubCategoryRepository, EfSubCategoryRepository>();
            services.AddScoped<ITagRepository, EfTagRepository>();

            services.AddScoped<ICategoryApplicationService, CategoryApplicationService>();
            services.AddScoped<ISubCategoryApplicationService, SubCategoryApplicationService>();
            services.AddScoped<ITagApplicationService, TagApplicationService>();

            // Auto Mapper Configurations  
            var mappingConfig = new MapperConfiguration(mc => {
                mc.AddProfile(new AutoMapping());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
        }
    }
}
