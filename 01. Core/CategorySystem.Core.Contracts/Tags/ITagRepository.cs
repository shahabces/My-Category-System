﻿using CategorySystem.Core.Entities.Tags;
using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.Contracts.Tags
{
    public interface ITagRepository
    {
        Tag Find(int id);
        Tag Add(Tag tag);
        List<Tag> GetAll();
    }
}
