﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.Contracts.Tags.Dtos
{
    public class TagDto
    {
        public string Name { get; set; }
    }

    public class TagResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
