﻿using CategorySystem.Core.Contracts.Tags.Dtos;
using System.Collections.Generic;

namespace CategorySystem.Core.Contracts.Tags
{
    public interface ITagApplicationService
    {
        TagResult AddTag(TagDto tagDto);
        List<TagResult> GetAllTags();
    }
}
