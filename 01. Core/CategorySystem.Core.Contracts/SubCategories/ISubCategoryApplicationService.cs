﻿using CategorySystem.Core.Contracts.SubCategories.Dtos;

namespace CategorySystem.Core.Contracts.SubCategories
{
    public interface ISubCategoryApplicationService
    {
        SetTagForSubCategoryResult SetTagForSubCategory(SetTagForSubCategoryDto setTagForSubCategoryDto);

    }
}
