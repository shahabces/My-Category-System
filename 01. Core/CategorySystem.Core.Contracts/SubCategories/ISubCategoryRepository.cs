﻿using CategorySystem.Core.Entities.SubCategories;
using CategorySystem.Core.Entities.Tags;
using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.Contracts.SubCategories
{
    public interface ISubCategoryRepository
    {
        SubCategory SetTag(int id, Tag tag);
    }
}
