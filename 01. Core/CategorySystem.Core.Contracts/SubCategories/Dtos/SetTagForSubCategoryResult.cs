﻿using CategorySystem.Core.Entities.Tags;

namespace CategorySystem.Core.Contracts.SubCategories.Dtos
{
    public class SetTagForSubCategoryResult
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Tag Tag { get; set; }
    }
}
