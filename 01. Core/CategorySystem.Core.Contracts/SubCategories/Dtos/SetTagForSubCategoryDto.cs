﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.Contracts.SubCategories.Dtos
{
    public class SetTagForSubCategoryDto
    {
        public int Id { get; set; }
        public int TagId { get; set; }
    }
}
