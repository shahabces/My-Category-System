﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.Contracts.Categories.Dtos
{
    public class SubCategoryResult
    {
        public string Name { get; set; }
    }
}
