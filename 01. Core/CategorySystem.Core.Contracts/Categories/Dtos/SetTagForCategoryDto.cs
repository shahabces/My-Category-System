﻿using System.Collections.Generic;

namespace CategorySystem.Core.Contracts.Categories.Dtos
{
    public class SetTagForCategoryDto
    {
        public int Id { get; set; }
        public int TagId { get; set; }
    }
}
