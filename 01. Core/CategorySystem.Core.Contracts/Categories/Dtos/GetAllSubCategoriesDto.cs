﻿namespace CategorySystem.Core.Contracts.Categories.Dtos
{
    public class GetAllSubCategoriesDto
    {
        public int CategoryId { get; set; }
    }
}
