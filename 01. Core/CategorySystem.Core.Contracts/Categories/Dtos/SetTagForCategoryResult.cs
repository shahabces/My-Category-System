﻿using CategorySystem.Core.Entities.Tags;
using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.Contracts.Categories.Dtos
{
    public class SetTagForCategoryResult
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Tag Tag { get; set; }
    }
}
