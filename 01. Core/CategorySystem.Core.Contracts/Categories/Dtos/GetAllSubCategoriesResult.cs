﻿using System.Collections.Generic;

namespace CategorySystem.Core.Contracts.Categories.Dtos
{
    public class GetAllSubCategoriesResult
    {
        public List<SubCategoryResult> SubCategoryResults { get; set; }

    }
}
