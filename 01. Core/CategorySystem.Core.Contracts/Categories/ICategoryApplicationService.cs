﻿using CategorySystem.Core.Contracts.Categories.Dtos;
using CategorySystem.Core.Entities.Categories;
using CategorySystem.Core.Entities.Tags;

namespace CategorySystem.Core.Contracts.Categories
{
    public interface ICategoryApplicationService
    {

        SetTagForCategoryResult SetTagForCategory(SetTagForCategoryDto setTagForCategoryDto);

        GetAllSubCategoriesResult GetAllSubCategories(GetAllSubCategoriesDto getAllSubCategoriesDto);
    }
}
