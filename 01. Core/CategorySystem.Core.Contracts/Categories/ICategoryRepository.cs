﻿using CategorySystem.Core.Entities.Categories;
using CategorySystem.Core.Entities.SubCategories;
using CategorySystem.Core.Entities.Tags;
using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.Contracts.Categories
{
    public interface ICategoryRepository
    {
        Category SetTag(int id, Tag tag);
        List<SubCategory> GetAllSubCategories(int id);
    }
}
