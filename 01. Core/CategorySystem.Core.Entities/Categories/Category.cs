﻿using CategorySystem.Core.Entities.Tags;
using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.Entities.Categories
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int? TagId { get; set; }
        public Tag Tag { get; set; }
    }
}
