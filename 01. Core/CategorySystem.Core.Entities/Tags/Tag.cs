﻿using CategorySystem.Core.Entities.SubCategories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.Entities.Tags
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<SubCategory> SubCategories { get; set; }
    }
}
