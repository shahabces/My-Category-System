﻿using AutoMapper;
using CategorySystem.Core.Contracts.SubCategories;
using CategorySystem.Core.Contracts.SubCategories.Dtos;
using CategorySystem.Core.Contracts.Tags;
using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.ApplicationServices.SubCategories
{
    public class SubCategoryApplicationService : ISubCategoryApplicationService
    {
        private readonly ISubCategoryRepository _subCategoryRepository;
        private readonly ITagRepository _tagRepository;
        private readonly IMapper _mapper;

        public SubCategoryApplicationService(ISubCategoryRepository subCategoryRepository,
            ITagRepository tagRepository,
            IMapper mapper
            )
        {
            _subCategoryRepository = subCategoryRepository;
            _tagRepository = tagRepository;
            _mapper = mapper;
        }
        public SetTagForSubCategoryResult SetTagForSubCategory(SetTagForSubCategoryDto setTagForSubCategoryDto)
        {
            var tag = _tagRepository.Find(setTagForSubCategoryDto.TagId);

            if (tag == null)
            {
                throw new Exception("Tag not found");
            }

            var category = _subCategoryRepository.SetTag(setTagForSubCategoryDto.Id, tag);
            return _mapper.Map<SetTagForSubCategoryResult>(category);
        }
    }
}
