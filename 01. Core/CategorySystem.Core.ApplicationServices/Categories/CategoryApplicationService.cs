﻿using AutoMapper;
using CategorySystem.Core.Contracts.Categories;
using CategorySystem.Core.Contracts.Categories.Dtos;
using CategorySystem.Core.Contracts.Tags;
using CategorySystem.Core.Entities.Categories;
using CategorySystem.Core.Entities.Tags;
using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.ApplicationServices.Categories
{
    public class CategoryApplicationService : ICategoryApplicationService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly ITagRepository _tagRepository;
        private readonly IMapper _mapper;

        public CategoryApplicationService(ICategoryRepository categoryRepository,
            ITagRepository tagRepository,
            IMapper mapper
            )
        {
            _categoryRepository = categoryRepository;
            _tagRepository = tagRepository;
            _mapper = mapper;
        }

        public SetTagForCategoryResult SetTagForCategory(SetTagForCategoryDto setTagForCategoryDto)
        {
            var tag = _tagRepository.Find(setTagForCategoryDto.TagId);

            if (tag == null)
            {
                throw new Exception("Tag not found");
            }

            var category = _categoryRepository.SetTag(setTagForCategoryDto.Id, tag);
            return _mapper.Map<SetTagForCategoryResult>(category);
        }

        public GetAllSubCategoriesResult GetAllSubCategories(GetAllSubCategoriesDto getAllSubCategoriesDto)
        {
            var subCategories = _categoryRepository.GetAllSubCategories(getAllSubCategoriesDto.CategoryId);
            return new GetAllSubCategoriesResult
            {
                SubCategoryResults = _mapper.Map<List<SubCategoryResult>>(subCategories)
            };
        }
    }
}
