﻿using AutoMapper;
using CategorySystem.Core.Contracts.Tags;
using CategorySystem.Core.Contracts.Tags.Dtos;
using CategorySystem.Core.Entities.Tags;
using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.ApplicationServices.Tags
{
    public class TagApplicationService : ITagApplicationService
    {
        private readonly ITagRepository _tagRepository;
        private readonly IMapper _mapper;

        public TagApplicationService(ITagRepository tagRepository, IMapper mapper)
        {
            _tagRepository = tagRepository;
            _mapper = mapper;
        }
        public TagResult AddTag(TagDto tagDto)
        {
            var tag = _mapper.Map<Tag>(tagDto);
            tag = _tagRepository.Add(tag);
            return _mapper.Map<TagResult>(tag);
        }

        public List<TagResult> GetAllTags()
        {
            var allTags = _tagRepository.GetAll();
            return _mapper.Map<List<TagResult>>(allTags);
        }
    }
}
