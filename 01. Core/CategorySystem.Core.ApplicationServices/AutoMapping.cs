﻿using AutoMapper;
using CategorySystem.Core.Contracts.Categories.Dtos;
using CategorySystem.Core.Contracts.SubCategories.Dtos;
using CategorySystem.Core.Contracts.Tags.Dtos;
using CategorySystem.Core.Entities.Categories;
using CategorySystem.Core.Entities.SubCategories;
using CategorySystem.Core.Entities.Tags;
using System;
using System.Collections.Generic;
using System.Text;

namespace CategorySystem.Core.ApplicationServices
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<TagDto, Tag>();
            CreateMap<Tag, TagResult>();

            CreateMap<Category, SetTagForCategoryResult>();
            CreateMap<SubCategory, SetTagForSubCategoryResult>();

            CreateMap<SubCategory, SubCategoryResult>();
        }
    }
}
